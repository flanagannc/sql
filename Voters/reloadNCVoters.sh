# This script assumes that it will be run out of a specific directory, edit
# the sql script to adjust that before running
# cleanup the compressed data file
rm ncvoter_statewide.zip
wget https://s3.amazonaws.com/dl.ncsbe.gov/data/ncvoter_Statewide.zip
# cleanup the uncompressed data file
rm ncvoter_Statewide.txt
unzip ncvoter_Statewide.zip
# for the sake of space cleanup again
rm ncvoter_Statewide.zip
# mysql -u kevin -p < /home/kevin/gitrepos/sql/Voters/reloadNC-Voters.sql
mysql -u kevin -p <<'EOF' 
use NCVoters;
truncate table voters;
LOAD DATA local INFILE '/home/kevin/Downloads/NCVoters/ncvoter_Statewide.txt' INTO TABLE voters FIELDS TERMINATED BY '  ' enclosed by '"'  IGNORE 1 LINES ;
select * from voters limit 20 
EOF


