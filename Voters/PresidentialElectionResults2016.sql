use Elections;
-- create table PresidentalElectionResults2016 (`County` int,`clinton` int,`trump` int,`johnson` int,`stein` int,`other` int,`totalvotes` bigint,`CountyName` varchar(20),`StateName` varchar(30),`StateAbbr` varchar(10),`dPct` varchar(10),`rPct` varchar(10),`leanD` varchar(10),`leanR` varchar(10),`otherPct` varchar(10),`dDRPct` varchar(10),`rDRPct` varchar(10),`State` varchar(10));
-- load data local infile '/home/kevin/gitrepos/sql/PresidentialElectionResults2016.csv' INTO table PresidentalElectionResults2016 fields terminated by ',' ignore 1 lines ;
-- drop table PresidentalElectionResults2016
-- select StateName, CountyName, clinton, trump, johnson, stein, other, totalvotes from PresidentalElectionResults2016  where StateAbbr = 'NC' and CountyName = 'Wake';
-- select StateName, CountyName, clinton, trump, johnson, stein, other, totalvotes from PresidentalElectionResults2016  where StateAbbr = 'MA' and CountyName = 'Worcester' 
select StateName, CountyName, clinton as Hillary, trump as Donald, johnson as Gary, stein as Jill, other, totalvotes from PresidentalElectionResults2016  WHERE (StateAbbr ='NC' OR StateAbbr = 'MA' OR StateAbbr = 'NE') AND (CountyName ='Wake' OR CountyName ='Worcester' or countyName = 'Mecklenburg' or countyName = 'Douglas')  order by totalvotes desc

